package service;

import ru.demid.newProject.converter.UserModelToUserDtoConverter;
import ru.demid.newProject.dao.UserDao;
import ru.demid.newProject.entity.UserModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import ru.demid.newProject.service.AuthService;
import ru.demid.newProject.service.DigestService;
import ru.demid.newProject.service.UserDTO;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class AuthServiceTest {
    @InjectMocks
    AuthService subj;

    @Mock
    UserDao userDao;
    @Mock
    DigestService digestService;
    @Mock
    UserModelToUserDtoConverter userDtoConverter;

    @Test
    public void auth_userNotFound() {
        when(digestService.hex("pasword")).thenReturn("hex");
        when(userDao.findByEmailAndHash("ptr@mail.ru", "hex")).thenReturn(null);

        UserDTO userDTO = subj.auth("ptr@mail.ru", "pasword");
        assertNull(userDTO);
        verify(digestService, times(1)).hex("pasword");
        verify(userDao, times(1)).findByEmailAndHash("ptr@mail.ru", "hex");
        verifyZeroInteractions(userDtoConverter);

    }

    @Test
    public void auth_userFound() {
        when(digestService.hex("pasword")).thenReturn("hex");
        UserModel userModel = new UserModel();
        userModel.setId(2);
        userModel.setEmail("ptr@mail.ru");
        when(userDao.findByEmailAndHash("ptr@mail.ru", "hex")).thenReturn(userModel);
        UserDTO userDTO = new UserDTO();
        userDTO.setEmail("ptr@mail.ru");
        userDTO.setId(2);
        when(userDtoConverter.convert(userModel)).thenReturn(userDTO);

        UserDTO user = subj.auth("ptr@mail.ru", "pasword");
        assertNotNull(user);
        assertEquals(userDTO, user);
        verify(digestService, times(1)).hex("pasword");
        verify(userDao, times(1)).findByEmailAndHash("ptr@mail.ru", "hex");
        verify(userDtoConverter, times(1)).convert(userModel);

    }

    @Test
    public void registration_rightUser() {
        when(digestService.hex("pasword")).thenReturn("hex");
        UserModel userModel = new UserModel();
        userModel.setId(2);
        userModel.setEmail("ptr@mail.ru");
        userModel.setName("Petr");
        userModel.setLastName("Petrovskiy");
        when(userDao.insert("ptr@mail.ru", "hex", "Petr", "Petrovskiy")).thenReturn(userModel);
        UserDTO userDTO = new UserDTO();
        userDTO.setEmail("ptr@mail.ru");
        userDTO.setId(2);
        userDTO.setLastName("Petrovskiy");
        userDTO.setName("Petr");
        when(userDtoConverter.convert(userModel)).thenReturn(userDTO);

        UserDTO user = subj.registration("ptr@mail.ru", "pasword", "Petr", "Petrovskiy");
        assertNotNull(user);
        assertEquals(userDTO, user);
        verify(digestService, times(1)).hex("pasword");
        verify(userDao, times(1)).insert("ptr@mail.ru", "hex", "Petr", "Petrovskiy");
        verify(userDtoConverter, times(1)).convert(userModel);
    }

    @Test
    public void registration_nullUser() {
        UserDTO user = subj.registration(null, null, null, null);

        assertNull(user);
        verify(digestService, times(0)).hex("");
        verify(userDao, times(0)).insert(null, null, null, null);
        verify(userDtoConverter, times(0)).convert(null);
    }

    @Test
    public void registration_withBlankLetterUser() {
        UserDTO user = subj.registration("", "", " ", "");
        UserModel userModel = null;
        assertNull(user);
        verify(digestService, times(0)).hex("");
        verify(userDao, times(0)).insert(null, null, null, null);
        verify(userDtoConverter, times(0)).convert(userModel);
    }
}