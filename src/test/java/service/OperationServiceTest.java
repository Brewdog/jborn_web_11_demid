package service;

import ru.demid.newProject.converter.OperationModelToOperationDTO;
import ru.demid.newProject.dao.OperationDAO;
import ru.demid.newProject.entity.Category;
import ru.demid.newProject.entity.OperationModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import ru.demid.newProject.service.OperationDTO;
import ru.demid.newProject.service.OperationService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class OperationServiceTest {
    @InjectMocks
    OperationService subj;

    @Mock
    OperationDAO oprDAO;

    @Mock
    OperationModelToOperationDTO converterToDTO;

    @Test
    public void viewByCategoryAndDate_NotFound() {

        when(oprDAO.reportByDate(1, "06-08-2019", "08-08-2019")).thenReturn(null);

        List <OperationDTO> list = subj.viewByCategoryAndDate(1, "06-08-2019", "08-08-2019");
        assertNull(list);
        verifyZeroInteractions(converterToDTO);
    }

    @Test
    public void viewByCategoryAndDate_RegularUse() {
        OperationModel operationModel = new OperationModel();
        operationModel.setSum(5000);
        operationModel.setCategories(Arrays.asList(new Category("Food")));
        List<OperationModel> listOM = new ArrayList<>();
        listOM.add(operationModel);
        when(oprDAO.reportByDate(1, "06-08-2019", "08-08-2019")).thenReturn(listOM);

        OperationDTO operationDTO = new OperationDTO();
        operationDTO.setSum(5000);
        when(converterToDTO.convert(operationModel)).thenReturn(operationDTO);
        List <OperationDTO> listDTO = new ArrayList<>();
        listDTO.add(operationDTO);

         List<OperationDTO> list = subj.viewByCategoryAndDate(1, "06-08-2019", "08-08-2019");

        assertNotNull(list);
        assertEquals(list, listDTO);
        verify(oprDAO, times(1)).reportByDate(1, "06-08-2019", "08-08-2019");
        verify(converterToDTO, times(1)).convert(operationModel);
    }
}