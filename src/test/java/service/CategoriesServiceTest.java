package service;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import ru.demid.newProject.dao.CategoriesDAO;
import ru.demid.newProject.exception.CustomException;
import ru.demid.newProject.service.CategoriesService;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CategoriesServiceTest {
    @Rule
    public ExpectedException ex = ExpectedException.none();

    @InjectMocks
    CategoriesService subj;

    @Mock
    CategoriesDAO categoriesDAO;

    @Test
    public void view_regularUse() {
        HashMap<Long, String> hashMap = new HashMap<>();
        hashMap.put((long) 1, "food");

        when(categoriesDAO.getAllCategories()).thenReturn(hashMap);

        Map<Long, String> view = subj.view();
        assertNotNull(view);
        assertEquals(view, hashMap);

    }

    @Test(expected = CustomException.class)
    public void view_NPE() {

        when(categoriesDAO.getAllCategories()).thenReturn(null);
        subj.view();
    }

    @Test
    public void setCategories_nullInput() {

        ex.expect(CustomException.class);
        ex.expectMessage("ID or name will not be empty");

        subj.setCategories(1, " ");
    }

    @Test
    public void setCategories_wrongIdInput() {
        when(categoriesDAO.rename(20, "noname")).thenReturn(null);
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        subj.setCategories(20, "noname");
        verify(categoriesDAO, times(1)).rename(20, "noname");
        assertEquals("Can't remove data from DB.", outContent.toString());
    }

    @Test
    public void delCategories_wrongIdInput() {
        when(categoriesDAO.remove(20)).thenReturn(false);
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        subj.delCategories(20);
        verify(categoriesDAO, times(1)).remove(20);
        assertEquals("DB error.", outContent.toString());
    }

    @Test
    public void addCategories() {
    }
}