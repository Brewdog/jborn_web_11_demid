package dao;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import ru.demid.newProject.SpringContext;
import ru.demid.newProject.dao.UserDao;
import ru.demid.newProject.entity.UserModel;
import ru.demid.newProject.exception.CustomException;

import javax.persistence.PersistenceException;
import java.util.UUID;

import static org.junit.Assert.assertEquals;

public class UserDaoTest {

    UserDao subj;

    @Before
    public void setUp() throws Exception {
        System.setProperty("jdbcUrl", "jdbc:h2:mem:test_mem" + UUID.randomUUID().toString());
        System.setProperty("jdbcUser", "sa");
        System.setProperty("jdbcPassword", "");
        System.setProperty("liquibaseFile", "liquibase_test.xml");
        ApplicationContext applicationContext = SpringContext.getContext();

        subj = applicationContext.getBean(UserDao.class);
    }

    @After
    public void after() {
        SpringContext.context = null;
    }


    @Test
    public void findByEmailAndHash() {
        UserModel user = subj.findByEmailAndHash("vano@mail.ru", "c8837b23ff8aaa8a2dde915473ce0991");

        assertEquals("vano@mail.ru", user.getEmail());
        assertEquals("c8837b23ff8aaa8a2dde915473ce0991", user.getPassword());
        assertEquals("Ванька", user.getName());
    }


    @Test(expected = CustomException.class)
    public void findByEmailAndHash_Not_Found() {
        UserModel user = subj.findByEmailAndHash("ivano@mail.ru", "c8837b23ff8aaa8a2dde915473ce0991");

    }


    @Test(expected = PersistenceException.class)
    public void insert_NotUniq() {
        subj.insert("vano@mail.ru", "c8837b23ff8aaa8a2dde915473ce0991", "Иван", "");
    }


    @Test
    public void insert_Regular() {
        UserModel user = subj.insert("vanoFF@mail.ru", "c8837b23ff8aaa8a2dde915473ce0991", "Иван", "");

        assertEquals("vanoFF@mail.ru", user.getEmail());
        assertEquals("c8837b23ff8aaa8a2dde915473ce0991", user.getPassword());
        assertEquals("Иван", user.getName());
    }
}