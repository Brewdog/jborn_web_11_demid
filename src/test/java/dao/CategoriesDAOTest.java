package dao;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import ru.demid.newProject.SpringContext;
import ru.demid.newProject.dao.CategoriesDAO;
import ru.demid.newProject.exception.CustomException;

import java.util.Map;
import java.util.UUID;

import static org.junit.Assert.*;

public class CategoriesDAOTest {
    CategoriesDAO subj;

    @Before
    public void setUp() throws Exception {

        System.setProperty("jdbcUrl", "jdbc:h2:mem:test_mem" + UUID.randomUUID().toString());
        System.setProperty("jdbcUser", "sa");
        System.setProperty("jdbcPassword", "");
        System.setProperty("liquibaseFile", "liquibase_test_Categories.xml");

        ApplicationContext context = SpringContext.getContext();
        subj = context.getBean(CategoriesDAO.class);
    }

    @After
    public void after() {
        SpringContext.context = null;
    }

    @Test
    public void getCategoriesRegularUse() {
        Map<Long, String> categories = subj.getAllCategories();

        assertNotNull(categories);
        assertEquals(7, categories.size());
        assertTrue(categories.containsValue("test"));
        assertTrue(categories.containsKey(99L));
    }

    @Test
    public void renameRegularUse() {
        subj.rename(99, "test1");
        String name1 = subj.getCategoryById(99).getName();

        assertEquals("test1", name1);
    }

    @Test
    public void renameWrongId() {
        String name = subj.rename(55, "test1");

        assertNull(name);

    }


    @Test
    public void removeRegularUse() {

        assertTrue(subj.remove(99));
    }

    @Test
    public void removeWrongID() {

        assertFalse(subj.remove(55));
    }

    @Test
    public void addingCategoryRegular() {
        long result = subj.addingCategory("test2");

        assertEquals(100, result);

    }

    @Test(expected = CustomException.class)
    public void addingCategoryNotUniqName() {

        long test = subj.addingCategory("test");
    }
}