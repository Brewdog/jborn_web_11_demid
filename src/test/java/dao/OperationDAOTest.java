package dao;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.context.ApplicationContext;
import ru.demid.newProject.SpringContext;
import ru.demid.newProject.dao.OperationDAO;
import ru.demid.newProject.entity.OperationModel;
import ru.demid.newProject.exception.CustomException;

import javax.persistence.PersistenceException;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

public class OperationDAOTest {
    OperationDAO subj;

    @Rule
    public ExpectedException ex = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
        System.setProperty("jdbcUrl", "jdbc:h2:mem:test_mem" + UUID.randomUUID().toString());
        System.setProperty("jdbcUser", "sa");
        System.setProperty("jdbcPassword", "");
        System.setProperty("liquibaseFile", "liquibase_test_Operation.xml");
        ApplicationContext context = SpringContext.getContext();
        subj = context.getBean(OperationDAO.class);
    }

    @After
    public void after() {
        SpringContext.context = null;
    }

    @Test
    public void reportByDateRegularUse() {
        List<OperationModel> list = subj.reportByDate(1, "06-08-2019", "11-11-2019");

        assertNotNull(list);
        assertFalse(list.isEmpty());
        assertEquals(5, list.size());
    }

    @Test
    public void reportByDateWrongHolder() {
        List<OperationModel> list = subj.reportByDate(88, "06-08-2019", "11-11-2019");

        assertNotNull(list);
        assertTrue(list.isEmpty());
    }


    @Test
    public void addOperation() {
        OperationModel operationModel = subj.addOperation(11L, 99L, null, 9999L, "19-19-2019", null, null);

        assertNotNull(operationModel);
        assertEquals(9999, operationModel.getSum());
//        assertEquals(99, operationModel.getSender().getId());
        assertNull(operationModel.getComment());
    }

    @Test
    public void addOperationWrongIdSenderAccount() {
        ex.expect(CustomException.class);
        ex.expectMessage("User not found");

        subj.addOperation(11, 55L, null, 9999, "19-19-2019", null, null);
    }

    @Test
    public void addOperationWrongHolderId() {
//        ex.expect(PersistenceException.class);
//        ex.expectMessage("Error with transaction of withdrawing");

        subj.addOperation(33, 99L, null, 9999, "19-19-2019", null, null);
    }

    @Test
    public void addOperationWrongSum() {
        ex.expect(PersistenceException.class);
//        ex.expectMessage("Error with adding transaction");

        subj.addOperation(11, 99L, null, 99999, "19-19-2019", null, null);
    }

}