package dao;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import ru.demid.newProject.SpringContext;
import ru.demid.newProject.dao.AccountDao;
import ru.demid.newProject.entity.Account;
import ru.demid.newProject.exception.CustomException;

import javax.persistence.PersistenceException;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

public class AccountDaoTest {
    private AccountDao subj;

    @Before
    public void setUp() throws Exception {

        System.setProperty("jdbcUrl", "jdbc:h2:mem:test_mem" + UUID.randomUUID().toString());
        System.setProperty("jdbcUser", "sa");
        System.setProperty("jdbcPassword", "");
        System.setProperty("liquibaseFile", "liquibase_test_Account.xml");
        ApplicationContext context = SpringContext.getContext();

        subj = context.getBean(AccountDao.class);
    }

    @After
    public void after() {
        SpringContext.context = null;
    }

    @Test
    public void getAccounts_Regular() {
        List<Account> accounts = subj.getAccounts(11L);

        assertNotNull(accounts);
        assertEquals(3, accounts.size());
        assertEquals("Тинькоф", accounts.get(1).getName()); //в принципе понятно что порядок не гарантирован и лучше тестировать на 1 записи
    }

    @Test
    public void getAccounts_NoAccounts() {
        List<Account> accounts = subj.getAccounts(13L);

        assertNotNull(accounts);
        assertTrue(accounts.isEmpty());
    }

    @Test
    public void getAccounts_NoHolder() {
        List<Account> accounts = subj.getAccounts(19L);

        assertNotNull(accounts);
        assertTrue(accounts.isEmpty());
    }

    @Test
    public void setAccounts_RegularUse() {
        Account account = subj.setAccounts(1, "VTB");

        assertNotNull(account);
        assertNotEquals(0, account.getId());
        assertEquals("VTB", account.getName());
        assertEquals(0, account.getBalance(), 0);
    }

    @Test(expected = CustomException.class)
    public void setAccounts_HolderIdNotFound() {
        subj.setAccounts(12, "VTB");
    }

    @Test(expected = PersistenceException.class)
    public void setAccounts_NotUniqName() {
        subj.setAccounts(11, "Наличка");
    }

    @Test
    public void testSetAccounts() {
        int rslt = subj.setAccounts(11, 99);

        assertEquals(1, rslt);
    }

    @Test
    public void testSetAccountsWrongHolderId() {
        int rslt = subj.setAccounts(99, 99);
        assertEquals(0, rslt);
    }

    @Test
    public void testSetAccountsWrongId() {
        int rslt = subj.setAccounts(11, 11);
        assertEquals(0, rslt);
    }

    @Test
    public void viewBalanceRegularUse() {
        long rslt = subj.viewBalance(11, 99);
        assertEquals(20000, rslt);
    }

    @Test
    public void viewBalanceWrongIdAccount() {
        long rslt = subj.viewBalance(11, 11);
        assertEquals(-1, rslt);
    }

    @Test
    public void viewBalanceWrongHolderId() {
        long rslt = subj.viewBalance(99, 99);
        assertEquals(-1, rslt);
    }
}