package ru.demid.newProject.menu;

public abstract class MenuEntry {
    private final String title;

    String getTitle() {
        return title;
    }

    protected MenuEntry(String title) {
        this.title = title;
    }

    public abstract void run();
}

