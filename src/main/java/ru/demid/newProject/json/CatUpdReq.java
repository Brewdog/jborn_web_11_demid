package ru.demid.newProject.json;

import lombok.Data;

@Data
public class CatUpdReq {
    private String name;
    private Long idCat;
}
