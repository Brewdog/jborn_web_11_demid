package ru.demid.newProject.json;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AccountAddResponse {
    private String name;
    private Float balance;
    private Long accountId;
}
