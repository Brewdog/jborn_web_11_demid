package ru.demid.newProject.json;

import lombok.AllArgsConstructor;
import lombok.Data;
import ru.demid.newProject.service.OperationDTO;

import java.util.List;

@Data
@AllArgsConstructor
public class TrnsViewRes {
    private List<OperationDTO> transactions;
}
