package ru.demid.newProject.json;

import lombok.Data;

@Data
public class CatDelReq {
    private Long idCat;
}
