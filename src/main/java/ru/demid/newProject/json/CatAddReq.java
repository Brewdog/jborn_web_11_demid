package ru.demid.newProject.json;

import lombok.Data;

@Data
public class CatAddReq {
    private String name;
}
