package ru.demid.newProject.json;

import lombok.Data;

@Data
public class AccDelReq {
    private Long idAcc;
}
