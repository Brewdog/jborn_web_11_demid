package ru.demid.newProject.json;

import lombok.Data;

@Data
public class AccUpdReq {
    private Long idAcc;
    private String name;
    private Long balance;
}
