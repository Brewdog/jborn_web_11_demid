package ru.demid.newProject.json;

import lombok.Data;
import ru.demid.newProject.entity.Category;

import java.util.List;

@Data
public class TrnsAddReq {
    private Long idSenderAcc;
    private Long idAccepterAcc;
    private Long summ;
    private String date;
    private String comment;
    private List<Category> categories;
}
