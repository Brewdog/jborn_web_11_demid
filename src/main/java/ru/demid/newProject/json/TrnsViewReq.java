package ru.demid.newProject.json;

import lombok.Data;

@Data
public class TrnsViewReq {
    private String startDate;
    private String finishDate;
}
