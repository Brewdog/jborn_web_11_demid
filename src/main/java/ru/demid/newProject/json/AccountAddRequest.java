package ru.demid.newProject.json;

import lombok.Data;

@Data
public class AccountAddRequest {
    private String name;
}
