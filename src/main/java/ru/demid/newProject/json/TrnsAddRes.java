package ru.demid.newProject.json;

import lombok.AllArgsConstructor;
import lombok.Data;
import ru.demid.newProject.service.OperationDTO;

@Data
@AllArgsConstructor
public class TrnsAddRes {
    private OperationDTO result;
}
