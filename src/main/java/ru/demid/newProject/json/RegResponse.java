package ru.demid.newProject.json;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RegResponse {
    private String name;
    private String email;
    private Long userId;
}
