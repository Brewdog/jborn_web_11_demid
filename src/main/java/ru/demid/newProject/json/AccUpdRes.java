package ru.demid.newProject.json;

import lombok.AllArgsConstructor;
import lombok.Data;
import ru.demid.newProject.entity.Account;

import java.util.List;

@Data
@AllArgsConstructor
public class AccUpdRes {
    private List<Account> accModels;
    private String message;
}
