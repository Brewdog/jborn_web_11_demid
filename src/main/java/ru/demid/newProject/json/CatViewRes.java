package ru.demid.newProject.json;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Map;

@Data
@AllArgsConstructor
public class CatViewRes {
    Map<Long, String> categories;
}
