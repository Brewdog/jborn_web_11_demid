package ru.demid.newProject;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class SpringContext {
    public static ApplicationContext context;
    public static ApplicationContext getContext(){
        if (context==null){
            try {
                Class.forName("org.postgresql.Driver");
            }catch (ClassNotFoundException ignored){}
            context = new AnnotationConfigApplicationContext(JpaConfiguration.class);
        }
        return context;
    }
}
