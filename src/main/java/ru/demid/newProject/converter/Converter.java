package ru.demid.newProject.converter;

public interface Converter<S, T> {
    T convert(S source);
}
