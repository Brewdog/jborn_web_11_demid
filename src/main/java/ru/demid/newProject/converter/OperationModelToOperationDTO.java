package ru.demid.newProject.converter;

import org.springframework.stereotype.Service;
import ru.demid.newProject.entity.OperationModel;
import ru.demid.newProject.service.OperationDTO;

@Service
public class OperationModelToOperationDTO implements Converter<OperationModel, OperationDTO> {
    @Override
    public OperationDTO convert(OperationModel source) {
        OperationDTO operationDTO = new OperationDTO();
        if (source.getAcceptor() != null) {
            operationDTO.setAcceptor(source.getAcceptor().getId());
        }
        if (source.getComment() != null) {
            operationDTO.setComment(source.getComment());
        }
        operationDTO.setDate(source.getDate());
        operationDTO.setId(source.getId());
        if (source.getSender() != null) {
            operationDTO.setSender(source.getSender().getId());
        }
        operationDTO.setSum(source.getSum());
        operationDTO.setCategories(source.getCategories());
        return operationDTO;
    }
}
