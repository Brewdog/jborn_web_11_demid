package ru.demid.newProject.converter;

import org.springframework.stereotype.Service;
import ru.demid.newProject.entity.UserModel;
import ru.demid.newProject.service.UserDTO;

@Service
public class UserModelToUserDtoConverter implements Converter<UserModel, UserDTO> {
    @Override
    public UserDTO convert(UserModel source) {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(source.getId());
        userDTO.setEmail(source.getEmail());
        userDTO.setName(source.getName());
        userDTO.setLastName(source.getLastName());
        return userDTO;
    }
}
