package ru.demid.newProject.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.ApplicationContext;
import ru.demid.newProject.SpringContext;
import ru.demid.newProject.controller.AuthController;
import ru.demid.newProject.controller.Controller;
import ru.demid.newProject.controller.RegistrationController;
import ru.demid.newProject.controller.SecureController;
import ru.demid.newProject.json.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class MainServlet extends HttpServlet {
    private ObjectMapper om = new ObjectMapper();
    private Map<String, Controller> controllers = new HashMap<>();
    private Map<String, SecureController> secureControllers = new HashMap<>();

    public MainServlet() {
        ApplicationContext context = SpringContext.getContext();
        for (String bean : context.getBeanNamesForType(Controller.class)) {
            controllers.put(bean, context.getBean(bean, Controller.class));
        }
        for (String bean : context.getBeanNamesForType(SecureController.class)) {
            secureControllers.put(bean, context.getBean(bean, SecureController.class));
        }

    }

    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String uri = req.getRequestURI();
        res.setContentType("application/json; charset=UTF-8");
        try {
            Controller controller = controllers.get(uri);
            if (controller != null) {
                if (controller instanceof AuthController) {
                    AuthController authController = (AuthController) controller;

                    AuthRequest authRequest = om.readValue(req.getInputStream(), authController.getRequestClass());
                    AuthResponse authResponse = authController.handle(authRequest);

                    if (authResponse == null) {
                        res.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                    } else {
                        HttpSession session = req.getSession();
                        session.setAttribute("userId", authResponse.getUserId());

                        om.writeValue(res.getWriter(), authResponse);
                    }

                }
                if (controller instanceof RegistrationController){
                    RegistrationController regController = (RegistrationController) controller;

                    RegRequst regRequst = om.readValue(req.getInputStream(), regController.getRequestClass());
                    RegResponse regResponse = regController.handle(regRequst);
                    if (regResponse == null){
                        res.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                        res.getWriter().write("Something wrong");
                    } else {
                        HttpSession session = req.getSession();
                        session.setAttribute("userId", regResponse.getUserId());

                        om.writeValue(res.getWriter(), "Have done! " + regResponse);
                    }
                }
                else {
                    om.writeValue(
                            res.getWriter(),
                            controller.handle(om.readValue(req.getInputStream(), controller.getRequestClass()))
                    );
                }
            } else {
                SecureController secureController = secureControllers.get(uri);
                if (secureController != null) {
                    HttpSession session = req.getSession();
                    Long userId = (Long) session.getAttribute("userId");
                    if (userId == null) {
                        res.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                    } else {
                        om.writeValue(
                                res.getWriter(),
                                secureController.handle(
                                        om.readValue(req.getInputStream(), secureController.getRequestClass()),
                                        userId
                                )
                        );
                    }
                } else {
                    res.setStatus(HttpServletResponse.SC_NOT_FOUND);
                }
            }
        } catch (Exception e) {
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            om.writeValue(res.getWriter(), new ExceptionResponse(e.getMessage()));
        }
    }
}
