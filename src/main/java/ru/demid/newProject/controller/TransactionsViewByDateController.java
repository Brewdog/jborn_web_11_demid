package ru.demid.newProject.controller;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.demid.newProject.json.TrnsViewReq;
import ru.demid.newProject.json.TrnsViewRes;
import ru.demid.newProject.service.OperationDTO;
import ru.demid.newProject.service.OperationService;

import java.util.List;

@Data
@RequiredArgsConstructor
@Service("/transView")
public class TransactionsViewByDateController implements SecureController<TrnsViewReq, TrnsViewRes> {
    private final OperationService service;

    @Override
    public TrnsViewRes handle(TrnsViewReq trnViewReq, Long userId) {
        List<OperationDTO> transactions = service.viewByCategoryAndDate(userId, trnViewReq.getStartDate(), trnViewReq.getFinishDate());
        if (!transactions.isEmpty()) {
            return new TrnsViewRes(transactions);
        }
        return null;
    }

    @Override
    public Class<TrnsViewReq> getRequestClass() {
        return TrnsViewReq.class;
    }
}
