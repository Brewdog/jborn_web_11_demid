package ru.demid.newProject.controller;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.demid.newProject.json.CatAddReq;
import ru.demid.newProject.json.CatAddRes;
import ru.demid.newProject.service.CategoriesService;

@Data
@RequiredArgsConstructor
@Service("/catAdd")
public class CategoryAddController implements SecureController<CatAddReq, CatAddRes> {
    private final CategoriesService service;

    @Override
    public CatAddRes handle(CatAddReq catAddReq, Long userId) {
        if (service.addCategories(catAddReq.getName()) > 0) {
            return new CatAddRes(service.view(), "Category has been added successfully");
        } else {
            return null;
        }
    }

    @Override
    public Class<CatAddReq> getRequestClass() {
        return CatAddReq.class;
    }
}
