package ru.demid.newProject.controller;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.demid.newProject.json.AccDelReq;
import ru.demid.newProject.json.AccDelRes;
import ru.demid.newProject.service.AccountService;

@Data
@RequiredArgsConstructor
@Service("/accDel")
public class AccountDeleteController implements SecureController<AccDelReq, AccDelRes> {
    private final AccountService accountService;

    @Override
    public AccDelRes handle(AccDelReq accDelReq, Long userId) {
        if (accountService.delAccount(accDelReq.getIdAcc(), userId)) {
            return new AccDelRes(accountService.viewAccounts(userId), "Account has been deleted successfully");
        }
        return null;
    }

    @Override
    public Class<AccDelReq> getRequestClass() {
        return AccDelReq.class;
    }
}
