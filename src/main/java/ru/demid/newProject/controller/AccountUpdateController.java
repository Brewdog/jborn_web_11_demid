package ru.demid.newProject.controller;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.demid.newProject.json.AccUpdReq;
import ru.demid.newProject.json.AccUpdRes;
import ru.demid.newProject.service.AccountService;

@Data
@RequiredArgsConstructor
@Service("/accUpd")
public class AccountUpdateController implements SecureController<AccUpdReq, AccUpdRes> {
    private final AccountService accountService;

    @Override
    public AccUpdRes handle(AccUpdReq accUpdReq, Long userId) {
        if (accountService.updateInfo(userId, accUpdReq.getIdAcc(), accUpdReq.getName(), accUpdReq.getBalance()) != null) {
            return new AccUpdRes(accountService.viewAccounts(userId), "Information has been updated");
        } else {
            return null;
        }
    }

    @Override
    public Class<AccUpdReq> getRequestClass() {
        return AccUpdReq.class;
    }
}
