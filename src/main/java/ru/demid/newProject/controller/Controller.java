package ru.demid.newProject.controller;

public interface Controller<REQ, RES> {
    RES handle(REQ req);

    Class<REQ> getRequestClass();
}
