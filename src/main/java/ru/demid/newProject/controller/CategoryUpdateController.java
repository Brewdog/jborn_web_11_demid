package ru.demid.newProject.controller;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.demid.newProject.json.CatUpdReq;
import ru.demid.newProject.json.CatUpdRes;
import ru.demid.newProject.service.CategoriesService;

@Data
@RequiredArgsConstructor
@Service("/catUpd")
public class CategoryUpdateController implements SecureController<CatUpdReq, CatUpdRes> {
    private final CategoriesService service;

    @Override
    public CatUpdRes handle(CatUpdReq catUpdReq, Long userId) {
        if (service.setCategories(catUpdReq.getIdCat(), catUpdReq.getName()) != null) {
            return new CatUpdRes(service.view(), "Information has been updated");
        }
        return null;
    }

    @Override
    public Class<CatUpdReq> getRequestClass() {
        return CatUpdReq.class;
    }
}
