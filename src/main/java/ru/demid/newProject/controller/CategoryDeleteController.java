package ru.demid.newProject.controller;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.demid.newProject.json.CatDelReq;
import ru.demid.newProject.json.CatDelRes;
import ru.demid.newProject.service.CategoriesService;

@Data
@RequiredArgsConstructor
@Service("/catDel")
public class CategoryDeleteController implements SecureController<CatDelReq, CatDelRes> {
    private final CategoriesService service;

    @Override
    public CatDelRes handle(CatDelReq catDelReq, Long userId) {
        if (service.delCategories(catDelReq.getIdCat())) {
            return new CatDelRes(service.view(), "Category has been deleted successfully");
        }
        return null;
    }

    @Override
    public Class<CatDelReq> getRequestClass() {
        return CatDelReq.class;
    }
}
