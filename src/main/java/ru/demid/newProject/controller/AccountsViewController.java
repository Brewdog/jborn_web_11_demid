package ru.demid.newProject.controller;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.demid.newProject.entity.Account;
import ru.demid.newProject.json.AccountsViewRequest;
import ru.demid.newProject.json.AccountsViewResponse;
import ru.demid.newProject.service.AccountService;

import java.util.List;

@Data
@RequiredArgsConstructor
@Service("/accView")
public class AccountsViewController implements SecureController<AccountsViewRequest, AccountsViewResponse> {
    private final AccountService accountService;

    @Override
    public AccountsViewResponse handle(AccountsViewRequest accountsViewRequest, Long userId) {
        List<Account> list = accountService.viewAccounts(userId);
        if (list == null) {
            return null;
        }
        return new AccountsViewResponse(list);
    }

    @Override
    public Class<AccountsViewRequest> getRequestClass() {
        return AccountsViewRequest.class;
    }
}
