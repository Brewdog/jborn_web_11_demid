package ru.demid.newProject.controller;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.demid.newProject.json.CatViewReq;
import ru.demid.newProject.json.CatViewRes;
import ru.demid.newProject.service.CategoriesService;

import java.util.Map;

@Data
@RequiredArgsConstructor
@Service("/catView")
public class CategoriesViewController implements SecureController<CatViewReq, CatViewRes> {
    private final CategoriesService service;

    @Override
    public CatViewRes handle(CatViewReq catViewReq, Long userId) {
        Map<Long, String> view = service.view();
        if (view.isEmpty()) {
            return null;
        }
        return new CatViewRes(view);
    }

    @Override
    public Class<CatViewReq> getRequestClass() {
        return CatViewReq.class;
    }
}
