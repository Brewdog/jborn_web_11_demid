package ru.demid.newProject.controller;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.demid.newProject.json.TrnsAddReq;
import ru.demid.newProject.json.TrnsAddRes;
import ru.demid.newProject.service.OperationDTO;
import ru.demid.newProject.service.OperationService;

@Data
@RequiredArgsConstructor
@Service("/transAdd")
public class TransactionAddController implements SecureController<TrnsAddReq, TrnsAddRes> {
    private final OperationService service;

    @Override
    public TrnsAddRes handle(TrnsAddReq trnsAddReq, Long userId) {
        OperationDTO operationDTO = service.addNewOperation(userId,
                trnsAddReq.getIdSenderAcc(),
                trnsAddReq.getIdAccepterAcc(),
                trnsAddReq.getSumm(),
                trnsAddReq.getDate(),
                trnsAddReq.getComment(),
                trnsAddReq.getCategories()
        );
        if (operationDTO != null) {
            return new TrnsAddRes(operationDTO);
        }
        return null;
    }

    @Override
    public Class<TrnsAddReq> getRequestClass() {
        return TrnsAddReq.class;
    }
}
