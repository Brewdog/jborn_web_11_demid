package ru.demid.newProject.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.demid.newProject.json.RegRequst;
import ru.demid.newProject.json.RegResponse;
import ru.demid.newProject.service.AuthService;
import ru.demid.newProject.service.UserDTO;


@Service("/reg")
@RequiredArgsConstructor
public class RegistrationController implements Controller<RegRequst, RegResponse> {
    private final AuthService authService;

    @Override
    public RegResponse handle(RegRequst regRequst) {
        UserDTO userDTO = authService.registration(regRequst.getEmail(), regRequst.getPassword(), regRequst.getName(), regRequst.getLastName());
        if (userDTO != null) {
            return new RegResponse(userDTO.getName(), userDTO.getEmail(), userDTO.getId());
        }
        return null;
    }

    @Override
    public Class<RegRequst> getRequestClass() {
        return RegRequst.class;
    }
}
