package ru.demid.newProject.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.demid.newProject.json.AuthRequest;
import ru.demid.newProject.json.AuthResponse;
import ru.demid.newProject.service.AuthService;
import ru.demid.newProject.service.UserDTO;

@Service("/login")
@RequiredArgsConstructor
public class AuthController implements Controller<AuthRequest, AuthResponse> {
    private final AuthService authService;

    @Override
    public AuthResponse handle(AuthRequest authRequest) {
        UserDTO userDTO = authService.auth(authRequest.getEmail(), authRequest.getPassword());
        if (userDTO != null) {
            return new AuthResponse(userDTO.getId(), userDTO.getEmail());

        }
        return null;
    }

    @Override
    public Class<AuthRequest> getRequestClass() {
        return AuthRequest.class;
    }
}
