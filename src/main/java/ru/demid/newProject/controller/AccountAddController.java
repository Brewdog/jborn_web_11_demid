package ru.demid.newProject.controller;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.demid.newProject.entity.Account;
import ru.demid.newProject.json.AccountAddResponse;
import ru.demid.newProject.json.AccountAddRequest;
import ru.demid.newProject.service.AccountService;

@Data
@RequiredArgsConstructor
@Service("/accAdd")
public class AccountAddController implements SecureController<AccountAddRequest, AccountAddResponse> {
    private final AccountService accountService;

    @Override
    public AccountAddResponse handle(AccountAddRequest accountAddRequest, Long userId) {
        Account account = accountService.makeAccount(accountAddRequest.getName(), userId);
        if (account == null) {
            return null;
        }
        return new AccountAddResponse(account.getName(), account.getBalance(), account.getId());
    }

    @Override
    public Class<AccountAddRequest> getRequestClass() {
        return AccountAddRequest.class;
    }
}
