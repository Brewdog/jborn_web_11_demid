package ru.demid.newProject.controller;

public interface SecureController<REQ, RES> {
    RES handle(REQ req, Long userId);

    Class<REQ> getRequestClass();
}
