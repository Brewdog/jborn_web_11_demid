package ru.demid.newProject.service;

import lombok.Data;
import ru.demid.newProject.entity.Account;

import java.util.List;

@Data
public class UserDTO {
    private long id;
    private String email;
    private String comment;
    private String name;
    private String lastName;
    private List<Account> accounts;
}
