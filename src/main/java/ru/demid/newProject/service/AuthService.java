package ru.demid.newProject.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.demid.newProject.converter.Converter;
import ru.demid.newProject.dao.UserDao;
import ru.demid.newProject.entity.UserModel;

@Service
@AllArgsConstructor
public class AuthService {
    private final UserDao userDao;
    private final DigestService digestService;
    private final Converter<UserModel, UserDTO> userDtoConverter;

    public UserDTO auth(String email, String password) {
        String hash = digestService.hex(password);
        UserModel userModel = userDao.findByEmailAndHash(email, hash);
        if (userModel == null) {
            return null;
        }
        return userDtoConverter.convert(userModel);
    }

    public UserDTO registration(String email, String password, String name, String lastName) {
        if (email == null || password == null || name == null || lastName == null) {
            return null;
        }
        if (email.contains(" ") || email.startsWith(" ") || email.equals("") || password.equals("") || name.equals("") || name.contains(" ") || lastName.contains(" ") || lastName.equals("")) {
            return null;
        }
        String hash = digestService.hex(password);

        UserModel userModel = userDao.insert(email, hash, name, lastName);
        if (userModel == null) {
            return null;
        }
        return userDtoConverter.convert(userModel);
    }
}
