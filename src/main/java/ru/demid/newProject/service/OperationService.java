package ru.demid.newProject.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.demid.newProject.converter.Converter;
import ru.demid.newProject.dao.OperationDAO;
import ru.demid.newProject.entity.Category;
import ru.demid.newProject.entity.OperationModel;
import ru.demid.newProject.exception.CustomException;
import ru.demid.newProject.exception.ServiceException;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OperationService {

    private final OperationDAO opDAO;
    private final Converter<OperationModel, OperationDTO> converterToDTO;

    public List<OperationDTO> viewByCategoryAndDate(long id, String startDate, String finishDate) {
        if (id <= 0) {
            throw new ServiceException("Wrong ID");
        }
        List<OperationDTO> listDTO;
        try {
            List<OperationModel> list = opDAO.reportByDate(id, startDate, finishDate);
            if (list == null) {
                return null;
            }
            listDTO = list.stream().map(converterToDTO::convert).collect(Collectors.toList());
        } catch (CustomException e) {
            throw new ServiceException(e);
        }

        return listDTO;
    }

    public OperationDTO addNewOperation(long idHolder, Long idSenderAccount, Long idAccepterAccount, long sum, String date, String comment) {
        if (idAccepterAccount == null && idSenderAccount == null) {
            throw new ServiceException("No accounts indicated");
        } else {
            if ((idAccepterAccount != null && idSenderAccount != null) && idAccepterAccount.equals(idSenderAccount)) {
                throw new ServiceException("Same account");
            }
        }
        try {
            OperationModel operationModel = opDAO.addOperation(idHolder, idSenderAccount, idAccepterAccount, sum, date, comment, Collections.emptyList());

            if (operationModel == null) {
                throw new ServiceException("Can't add this operation");
            }
            return converterToDTO.convert(operationModel);

        } catch (CustomException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    public OperationDTO addNewOperation(long idHolder, Long idSenderAccount, Long idAccepterAccount, long sum, String date, String comment, List<Category> categories) {
        if (categories == null || categories.isEmpty()) {
            return addNewOperation(idHolder, idSenderAccount, idAccepterAccount, sum, date, comment);
        } else {
            if (idAccepterAccount == null && idSenderAccount == null) {
                throw new ServiceException("No accounts indicated");
            } else {
                if ((idAccepterAccount != null && idSenderAccount != null) && idAccepterAccount.equals(idSenderAccount)) {
                    throw new ServiceException("Same account");
                }
            }
            try {
                return converterToDTO.convert(opDAO.addOperation(idHolder, idSenderAccount, idAccepterAccount, sum, date, comment, categories));
            } catch (CustomException e) {
                throw new ServiceException(e.getMessage());
            }
        }
    }
}
