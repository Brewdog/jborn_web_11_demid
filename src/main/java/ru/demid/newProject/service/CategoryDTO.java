package ru.demid.newProject.service;

import lombok.Data;


@Data
public class CategoryDTO {
    private String name;
    private Long idCategory;
}
