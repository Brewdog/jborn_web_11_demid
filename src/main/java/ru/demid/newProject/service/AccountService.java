package ru.demid.newProject.service;

import lombok.RequiredArgsConstructor;
import ru.demid.newProject.dao.AccountDao;
import ru.demid.newProject.entity.Account;
import org.springframework.stereotype.Service;
import ru.demid.newProject.exception.ServiceException;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AccountService {
    private final AccountDao accountDao;

    public List<Account> viewAccounts(long id) {
        return accountDao.getAccounts(id);
    }

    public Account makeAccount(String name, long idHolder) {
        if (viewAccounts(idHolder).stream().anyMatch(x -> x.getName().toLowerCase().equals(name.toLowerCase()))) {
            throw new ServiceException("Name already exist");
        }
        return accountDao.setAccounts(idHolder, name);
    }

    public boolean delAccount(long idAccount, long idHolder) {
        if (accountDao.setAccounts(idHolder, idAccount) == 1) {
            return true;
        }
        return false;
    }

    public Account updateInfo(long idHolder, long idAccount, String name, Long balance) {
        if (idHolder <= 0 || idAccount <= 0 || name == null) {
            return null;
        }
        Account account = new Account();
        if (balance == null) {
            if (accountDao.changeInformation(idHolder, idAccount, name)) {
                account.setId(idAccount);
                account.setName(name);
                return account;
            } return null;
        } else {
            if (accountDao.changeInformation(idHolder, idAccount, name, balance)) {
                account.setId(idAccount);
                account.setBalance(balance);
                account.setName(name);
                return account;
            } return null;
        }
    }
}


