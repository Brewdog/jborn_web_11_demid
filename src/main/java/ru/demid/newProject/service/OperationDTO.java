package ru.demid.newProject.service;

import lombok.Data;
import ru.demid.newProject.entity.Category;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Data
public class OperationDTO {
    private long id;
    private long sum;
    private long sender;
    private long acceptor;
    private long transaction;
    private String date;
    private String comment;
    private List<Category> categories;

    public void setCategories(Category category) {
        if (categories == null) {
            categories = new ArrayList<>(Arrays.asList(category));
        } else {
            categories.add(category);
        }
    }

    public void setCategories(List<Category> list) {
        categories = list;
    }
}
