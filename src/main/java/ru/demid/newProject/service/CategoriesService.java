package ru.demid.newProject.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.demid.newProject.dao.CategoriesDAO;
import ru.demid.newProject.exception.CustomException;
import ru.demid.newProject.exception.ServiceException;

import java.util.Map;

@Service
@RequiredArgsConstructor
public class CategoriesService {
    private final CategoriesDAO catDAO;

    public Map<Long, String> view() {
        Map<Long, String> categories = catDAO.getAllCategories();
        if (categories == null || categories.isEmpty()) {
            throw new CustomException("Empty list of categories.");
        }
        return categories;
    }

    public String setCategories(long id, String name) {
        if (id <= 0 || name == null || name.trim().equals("")) {
            throw new CustomException("ID or name will not be empty.");
        }
        String result = null;
        try {
            result = catDAO.rename(id, name);
            if (result == null || !result.equals(name)) {
                throw new ServiceException("Can't remove data from DB.");
            }
        } catch (ServiceException | CustomException e) {
            System.out.print(e.getMessage());
        }
        return result;
    }

    public boolean delCategories(long id) {
        boolean result = false;
        try {
            result = catDAO.remove(id);
            if (!result) {
                throw new ServiceException("DB error.");
            }

        } catch (CustomException | ServiceException e) {
            System.out.print(e.getMessage());
        }
        return result;
    }

    public long addCategories(String name) {
        if (name == null || name.trim().equals("")) {
            throw new ServiceException("Field Name can't be empty.");
        }
        if(catDAO.getAllCategories().values().stream().anyMatch(x-> x.equals(name))){
            throw new ServiceException("Category already exists.");
        }
        long result = 0;
        try {
            result = catDAO.addingCategory(name);
            if (result == -1) {
                throw new ServiceException("Error at adding category.");
            }
        } catch (CustomException e) {
            System.out.println(e.getMessage());
        }
        return result;
    }
}
