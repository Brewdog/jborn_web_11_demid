package ru.demid.newProject.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "operation")
@NamedQueries({
        @NamedQuery(
                name = "Operation.GetByDateAndCategories",
                query = "select o from OperationModel o " +
                        " left join o.acceptor " +
                        " left join o.sender " +
                        "where function('to_date', o.date, 'dd-MM-YYYY') between function('to_date', :start, 'dd-MM-YYYY') and function('to_date', :finish, 'dd-MM-YYYY') " +
                        "and o.sender.accountHolder.id = :id or o.acceptor.accountHolder.id = :id"),
        @NamedQuery(
                name = "Operation.findOperationById",
                query = "select o from OperationModel  o \n" +
                        "where o.id = :idOp \n" +
                        "      " +
                        "and (o.acceptor.accountHolder.id = :idHld or o.sender.accountHolder.id = :idHld)"
        )
})
public class OperationModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    private long sum;

    @JoinColumn(name = "sender")
    @ManyToOne
    private Account sender;

    @JoinColumn(name = "acceptor")
    @ManyToOne
    private Account acceptor;

    @Column
    private String date;

    @Column
    private String comment;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    @JoinTable(
            name = "category_to_operation",
            joinColumns = @JoinColumn(name = "operation_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "category_id", referencedColumnName = "id")
    )
    private List<Category> categories;
}
