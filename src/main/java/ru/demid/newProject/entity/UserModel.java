package ru.demid.newProject.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Data
@Entity
@Table(name = "holder")
@NamedQueries({
        @NamedQuery(name = "UserModel.findByEmailAndHash", query = "select u from UserModel u where u.email = :email and u.password = :password")
})
public class UserModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column()
    private String email;

    @Column
    private String password;

    @Column(name = "first_name")
    private String name;

    @Column(name = "second_name")
    private String lastName;

    @OneToMany(mappedBy = "accountHolder", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<Account> accounts;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserModel userModel = (UserModel) o;
        return id == userModel.id &&
                Objects.equals(email, userModel.email) &&
                Objects.equals(password, userModel.password);
    }

}
