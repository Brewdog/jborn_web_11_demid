package ru.demid.newProject.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "category")
@Data
@NamedQueries({
        @NamedQuery(name = "Category.findAll", query = "select c from Category c"),
        @NamedQuery(name = "Category.remove", query = "delete from Category where id = :id"),
        @NamedQuery(name = "Category.rename", query = "update Category set name = :name where id = :id")
})
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "type")
    private String name;

    public Category(String name) {
        this.name = name;
    }

    public Category() {
    }
}
