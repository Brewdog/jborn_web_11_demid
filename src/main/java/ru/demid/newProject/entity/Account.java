package ru.demid.newProject.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "account")
@NamedQueries({
        @NamedQuery(name = "Account.findAll", query = "select a from Account a where a.accountHolder.id = :idHolder")
})
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name = "balance")
    private float balance;

    @OneToMany(mappedBy = "sender", fetch = FetchType.EAGER)
    private List<OperationModel> sentOperations;

    @OneToMany(mappedBy = "acceptor", fetch = FetchType.EAGER)
    private List<OperationModel> receivedOperations;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "holder_id")
    private UserModel accountHolder;
}
