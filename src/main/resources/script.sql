create table holder
(
    id          SERIAL PRIMARY KEY,
    first_name  VARCHAR(255) NOT NULL,
    second_name VARCHAR(255) NULL,
    email       VARCHAR(255) NOT NULL,
    password    VARCHAR(255) NOT NULL
);
update holder
set password = md5(password);
create table account
(
    id        SERIAL PRIMARY KEY,
    name      VARCHAR(255) NOT NULL,
    balance   float        NOT NULL,
    holder_id int REFERENCES holder (id)
);
create table operation
(
    id       SERIAL PRIMARY KEY,
    sum      float        NOT NULL,
    sender   int          NULL references account (id),
    acceptor int          NULL references account (id),
    date     VARCHAR(255) NOT NULL,
    comment  VARCHAR(255) null
);
create table category
(
    id   SERIAL PRIMARY KEY,
    type VARCHAR(255) NULL
);
create table category_to_operation
(
    category_id  int REFERENCES category (id) ON DELETE CASCADE ON UPDATE CASCADE,
    operation_id int REFERENCES operation (id)
);

insert into holder(first_name, second_name, email, password)
values ('Иван', 'Иванов', 'ivanov@mail.ru', md5('123321'));
insert into holder(first_name, second_name, email, password)
values ('Андрей', 'Петров', 'petrov@gmail.com', md5('321123'));
insert into holder(first_name, second_name, email, password)
values ('Владимир', 'Сидоров', 'sidorov@bk.ru', md5('123654'));
insert into holder(first_name, second_name, email, password)
values ('Анна', 'Иванова', 'ivanova@mail.ru', md5('456321'));
insert into holder(first_name, second_name, email, password)
values ('Ирина', 'Михайлова', 'mikhailova@bk.ru', md5('456321'));

insert into account(name, balance, holder_id)
VALUES ('Сбербанк', 20000, 1);
insert into account(name, balance, holder_id)
VALUES ('Тинькоф', 50000, 1);
insert into account(name, balance, holder_id)
VALUES ('Наличка', 5000, 1);
insert into account(name, balance, holder_id)
VALUES ('Сбербанк', 100000, 2);
insert into account(name, balance, holder_id)
VALUES ('ВТБ', 70000, 3);
insert into account(name, balance, holder_id)
VALUES ('Наличка', 10000, 3);
insert into account(name, balance, holder_id)
VALUES ('Сбербанк', 50000, 4);
insert into account(name, balance, holder_id)
VALUES ('Сбербанк', 0, 5);

insert into operation(sum, sender, acceptor, date, comment)
VALUES (-250, 1, null, '07-08-2019', 'Аптека');
insert into operation(sum, sender, acceptor, date, comment)
VALUES (-500, 1, null, '07-08-2019', NULL);
insert into operation(sum, sender, acceptor, date, comment)
VALUES (-750, 5, NULL, '20-08-2019', 'Кино');
insert into operation(sum, sender, acceptor, date, comment)
VALUES (-550, 5, NULL, '12-08-2019', 'Домино');
insert into operation(sum, sender, acceptor, date, comment)
VALUES (-1000, 7, NULL, '07-08-2019', NULL);
insert into operation(sum, sender, acceptor, date, comment)
VALUES (50000, NULL, 7, '07-08-2019', 7);
insert into operation(sum, sender, acceptor, date, comment)
VALUES (-3000, 1, null, '08-08-2019', 'Ашан');
insert into operation(sum, sender, acceptor, date, comment)
VALUES (-550, 1, NULL, '05-08-2019', 'У дома');
insert into operation(sum, sender, acceptor, date, comment)
VALUES (-400, 1, NULL, '06-08-2019', 'У дома');
insert into operation(sum, sender, acceptor, date, comment)
VALUES (-200, 1, NULL, '06-08-2019', 'У дома');

insert into category(type)
values ('Health');
insert into category(type)
values ('Food');
insert into category(type)
values ('Salary');
insert into category(type)
values ('Entertainments');
insert into category(type)
values ('Investments');

insert into category_to_operation(category_id, operation_id)
VALUES (1, 1);
insert into category_to_operation(category_id, operation_id)
VALUES (2, 2);
insert into category_to_operation(category_id, operation_id)
VALUES (4, 3);
insert into category_to_operation(category_id, operation_id)
VALUES (4, 4);
insert into category_to_operation(category_id, operation_id)
VALUES (1, 4);
insert into category_to_operation(category_id, operation_id)
VALUES (3, 5);
insert into category_to_operation(category_id, operation_id)
VALUES (2, 7);
insert into category_to_operation(category_id, operation_id)
VALUES (2, 9);
insert into category_to_operation(category_id, operation_id)
VALUES (2, 11);
insert into category_to_operation(category_id, operation_id)
VALUES (2, 18);
insert into category_to_operation(category_id, operation_id)
VALUES (2, 19);

select h.first_name,
       h.second_name,
       a.name,
       a.balance
from holder as h
         join account as a on h.id = a.holder_id
where h.second_name like 'Иванов';

select h.first_name,
       h.second_name,
       count(a.balance),
       sum(a.balance)
from holder as h
         full outer join account as a on h.id = a.holder_id
group by h.second_name, h.first_name;

select sum(o.sum)   as Summs,
       count(o.sum) as Transaction,
       c.type

from operation o
         join category_to_operation cto on o.id = cto.operation_id
         join category c on cto.category_id = c.id
         join account a on o.sender = a.id
where a.holder_id = 1
  and to_date(o.date, 'DD-MM-YYY') between '05-08-2019'::date and '09-08-2019'::date
group by c.type;

select h.first_name,
       h.second_name,
       o.date,
       a.name,
       o.acceptor,
       o.sum,
       c.type
from holder as h
         join account a on h.id = a.holder_id
         join operation o on a.id = o.sender
         join category_to_operation cto on o.id = cto.operation_id
         join category c on cto.category_id = c.id
where date like '07-08-2019'
  and second_name like 'Иванов';
